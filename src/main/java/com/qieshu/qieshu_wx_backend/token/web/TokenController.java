package com.qieshu.qieshu_wx_backend.token.web;

import com.qieshu.qieshu_wx_backend.token.entity.TokenATO;
import com.qieshu.qieshu_wx_backend.token.service.TokenService;
import com.qieshu.qieshu_wx_backend.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import sun.applet.Main;

import javax.servlet.http.HttpServletRequest;

import static com.qieshu.qieshu_wx_backend.utils.HttpUtils.NOT_FOUND;
import static com.qieshu.qieshu_wx_backend.utils.HttpUtils.OK;

/**
 * ClassName TokenController
 * Descricption TOOD
 *
 * @Authorhaobaoyang
 * @Dage 2018/12/23  17:13
 * @VERSION 1.0
 **/

@RestController
@ResponseBody
public class TokenController {

@Autowired
public TokenService tokenService ;

    @GetMapping(  name = "生成解析token",  path = "/wx/qieshutoken" )
    public Result getToken( @RequestParam(value = "appkey" , required = false ) String appkey ,
                            @RequestParam(value = "refresh" , required = false) boolean refreshToken,
                             HttpServletRequest httpRequest  ){

        if( !TokenATO.appkey.equals(  appkey  )  ){
            return new Result( NOT_FOUND , "Invalid appkey ,the mini parogrm of appkey is different from token service " , null   );
        }
        String refer =  httpRequest.getHeader("Referer") ;
        //检查请求是否来自特定的微信小程序
        if( !tokenService.checkWxAPPID(  refer )  ){
            return new Result( NOT_FOUND , "Invalid appkey ,the mini parogrm of WxAPPID is different from token service "  , null   );
        }else{
            //像箧书服务器获取token
            if(  refreshToken == true || StringUtils.isEmpty(  TokenATO.token  )  ){
                if(!tokenService.getToken( appkey  ,TokenATO.token)  ){
                    return  new Result( NOT_FOUND , "Invalid appkey or seckey  , please check server Resource of appkey and seckey"  ) ;
                }
            }
            if(StringUtils.isEmpty(  TokenATO.token  )){
                return new Result( NOT_FOUND , "the server product token without empty" , null   );
            }
        }
        return new Result( OK , TokenATO.token  ) ;

    }




}
