package com.qieshu.qieshu_wx_backend.token.service;

import com.qieshu.qieshu_wx_backend.token.entity.TokenATO;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.Random;

/**
 * ClassName TokenServiceImpl
 * Descricption TOOD
 *
 * @Authorhaobaoyang
 * @Dage 2018/12/23  17:13
 * @VERSION 1.0
 **/

@Service
public class TokenServiceImpl implements TokenService {

    private static String url = "https://api.buyfull.cc/api/buyfulltoken";

    @Override
    public boolean getToken( String appkey ,String oldToken ) {
        if( !TokenATO.appkey.equals(  appkey  )  ){
            return false ;
        }
        return getQieshuToken( appkey ,oldToken );
    }

    //通过请求获取箧书解析token，缓存到本地
    private synchronized boolean getQieshuToken( String appkey , String oldToken   ){
        if(  TokenATO.token != oldToken  ){ //防止多线程
               return true ;
        }
        RestTemplate restTemplate = new RestTemplate();
        Random random = new Random();
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("nocache", random.nextInt())
                .queryParam("appkey",  TokenATO.appkey)
                .queryParam("seckey",  TokenATO.seckey)
                .queryParam("sandbox",  TokenATO.useSandbox)
                .queryParam("oldtoken",  TokenATO.token     );

        ResponseEntity<QSTokenVO> responseEntity = restTemplate.exchange(
                uriBuilder.toUriString(),
                HttpMethod.GET,
                 null,
                QSTokenVO.class

        );
        if ( responseEntity.getStatusCode() == HttpStatus.OK ) {
            QSTokenVO  result = responseEntity.getBody() ;
            switch (  result.getCode() ){

                case 200: //OK
                {
                    TokenATO.token = result.getToken() ;
                    break;
                }
                case 304: //现有Token未过期，可以继续使用
                {
                    break;
                }
                case 401: //Token过期，更新缓存
                {
                    TokenATO.token = result.getToken() ;
                    break;
                }
                case 404: //appkey & seckey 是错的
                {
                    TokenATO.token =  null ;
                    return false ;
                }
            }
        }

      return true ;
    }

    @Override
    public boolean checkWxAPPID(String referer) {
        if(!TokenATO.checkWxAPPID){
            return true ;
        }
        if( !StringUtils.isEmpty( referer )   ){
            if(referer.startsWith( TokenATO.checkWxPrefix  )){
                return  true ;
            }
        }
        return false;
    }

}
