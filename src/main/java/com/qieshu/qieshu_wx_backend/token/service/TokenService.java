package com.qieshu.qieshu_wx_backend.token.service;

public interface TokenService {
     /**
      *
      * 获取和刷新箧书服务器声波解析token
      * @param appkey
      * @param oldToken
      * @return
      */
     public boolean getToken( String appkey ,String oldToken ) ;

     /**
      * @deprecated   校验特定appid
      * @param referer  微信小程序访问连接
      * @return
      */
     public boolean checkWxAPPID( String referer   ) ;

     public static class QSTokenVO{

          private int code ;

          private String token ;

          public int getCode() {
               return code;
          }

          public void setCode(int code) {
               this.code = code;
          }

          public String getToken() {
               return token;
          }

          public void setToken(String token) {
               this.token = token;
          }
     }

}
