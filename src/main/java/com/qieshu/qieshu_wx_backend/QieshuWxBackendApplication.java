package com.qieshu.qieshu_wx_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QieshuWxBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(QieshuWxBackendApplication.class, args);
	}

}

