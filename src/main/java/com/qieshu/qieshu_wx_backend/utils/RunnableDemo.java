package com.qieshu.qieshu_wx_backend.utils;

import com.qieshu.qieshu_wx_backend.token.entity.TokenATO;
import com.qieshu.qieshu_wx_backend.token.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * ClassName TestDemo
 * Descricption TOOD
 *
 * @Authorhaobaoyang
 * @Dage 2018/12/26  10:32
 * @VERSION 1.0
 **/


public class RunnableDemo implements Runnable{

    private Thread t ;

    private String threadName ;

    @Autowired
    public TokenService tokenService ;

    public RunnableDemo(String name) {
        threadName = name;

        System.out.println("Creating " +  threadName );
    }

    public void run() {
        System.out.println("Running " +  threadName );
        System.out.println(    );
        boolean falg = tokenService.getToken("0de723ff5d0646e687d570e63268b963" , TokenATO.token ) ;

        System.out.println("Thread " +  threadName + " exiting."  +    "the status is = " + falg );


    }

    public void start () {
        System.out.println("Starting " +  threadName );
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }

}


