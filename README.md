buyfull_wx_backend
准备
 请和百蝠工作人员联系获取售前服务文档，并全部完成。如果只是想尝试一下SDK，可以跳过这一步。
开发者需要从百蝠官网(http://www.buyfull.cc) 或是开发服(http://sandbox.buyfull.cc) 申请帐号，用APPKEY和SECKEY来获得buyfulltoken
实现后台接口
此服务器实现了返回buyfull token的方法/wx/buyfulltoken，具体地址可在SDK初始化中定义。此方法接受两个参数appkey和refresh，返回缓存的buyfulltoken。
此项目仅为参考实现，不建议使用在正式服务器中，百蝠不对此项目做任何保证。你可以用任何熟悉的语言框架来实现相同的业务功能，可以集成布署也可以独立布署。
参照wx sdk中index.js中的配置

onLoad：
detector.init({     appKey:"121e87d73077403eadd9ab4fec2d9973",//请替换成自己的APPKEY
    buyfullTokenUrl:"https://sandbox.buyfull.cc/wx/buyfulltoken", //此URL请替换成自已的服务器地址，这个地址上需要布署此后台服务，用来返回buyfull token给buyfullsdk.js
    abortTimeout: 3000,
    detectTimeout: 5000,
debugLog: true,
});

测试
 用苹果电脑或是独立电箱播放testsound目录中的mp3音乐，在手机上测试识别结果
注意事项和常见问题：
1）请查看一下TokenServiceImpl.java中的注释，目前提供java ,go语言的参考实现，也可以参考main.go中的注释来实现其它语言的实现。
2）func main()中定义的环境变量只在这个DEMO中使用，你的实现代码可以完全无视这些变量。
3）请分清楚APPKEY和SECKEY是在正式服 www.buyfull.cc申请的还是在测试服sandbox.buyfull.cc 申请的。线下店帐号和APP帐号都要在同一平台上申请才能互相操作。在调用https://api.buyfull.cc/api/buyfulltoken 时要记得传入正确的sandbox值。
4）请确保网络通畅并且可以连接外网。
5）buyfulltoken会不定时过期，微信SDK发现buyfulltoken过期后会发出带有refresh的请求，这时服务器可以请求更新buyfulltoken并返回给微信SDK。buyfull token无需长期保存，没有过期时间策略，只要在内存中记录服务器返回值既可。每次请求https://api.buyfull.cc/api/buyfulltoken 时在oldtoken参数中传入内存中记录的buyfull token既可，如果没有就传空字符串。
 6) 建议检查WXAPPID以免后台服务被别的小程序冒用。
 7) 请至少在APP帐号下购买一个渠道后再进行测试，并且请在渠道中自行设定，自行设定，自行设定（重要的事情说三遍）识别结果，可以为任何字符串包括JSON。
 8) 请在调用https://api.buyfull.cc/api/buyfulltoken 时加入多线程或多进程锁，以确保同时只有一个请求会更新buyfulltoken，否则过多的请求引起百蝠服务器将你的服务暂时限制一段时间。

有疑问请联系QQ:164748776